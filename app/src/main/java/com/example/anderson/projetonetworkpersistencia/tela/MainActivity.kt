package com.example.anderson.projetonetworkpersistencia.tela

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.anderson.projetonetworkpersistencia.R
import com.example.anderson.projetonetworkpersistencia.aplication.MyApplication
import com.example.anderson.projetonetworkpersistencia.model.Personagem
import com.example.anderson.projetonetworkpersistencia.retrofit.ApiRetrofit
import com.example.anderson.projetonetworkpersistencia.util.Util
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (Util.validarConnection(this)) {
            callBackPersonagem()
        } else {
            getPersonagensBanco()
        }
    }

    fun callBackPersonagem() {

        val callBack = object : Callback<Personagem> {
            override fun onResponse(call: Call<Personagem>?, response: Response<Personagem>?) {

                val personagem = response?.body()

                if (personagem != null) {
                    montarComponentes(personagem)
                }
            }

            override fun onFailure(call: Call<Personagem>?, t: Throwable?) {
                getPersonagensBanco()
            }
        }

        ApiRetrofit.getPersonagem(callBack)

    }

    fun montarComponentes(personagem: Personagem) {

        txtValorNome.text = personagem?.nome
        txtValorApelido.text = personagem?.apelido
        txtValorSexo.text = personagem?.sexo
        txtValorRaca.text = personagem?.raca
        var urlImg = personagem?.image

        getImagem(urlImg)

        inserirPersonagem(personagem)
    }

    fun getImagem(urlImg: String) {
        Picasso
                .with(this)
                .load(urlImg)
                .into(imgPersonagem)
    }

    fun inserirPersonagem(personagem: Personagem) {

        MyApplication.database?.personagemDaoInterface()?.addPersonagem(personagem)
    }

    fun getPersonagensBanco() {

        val objetoPersonagem = MyApplication.database?.personagemDaoInterface()?.getPersonagens()

        if (objetoPersonagem != null) {

            montarComponentes(objetoPersonagem?.get(0))
            Toast.makeText(this@MainActivity, "Entrou Funcao consulta" , Toast.LENGTH_LONG).show()

        }

    }


}
