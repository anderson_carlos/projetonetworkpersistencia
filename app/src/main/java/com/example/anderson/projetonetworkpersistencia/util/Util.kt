package com.example.anderson.projetonetworkpersistencia.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.widget.Toast

/**
 * Created by Anderson on 04/08/2018.
 */
class Util {

    companion object {
        fun validarConnection(ctx: Context): Boolean {
            val conexao = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networks = conexao.allNetworks
            for (n in networks) {
                val info = conexao.getNetworkInfo(n)
                if (info.state == NetworkInfo.State.CONNECTED) {

                    return true
                }
            }
            return false
        }
    }
}