package com.example.anderson.projetonetworkpersistencia.aplication

import android.app.Application
import android.arch.persistence.room.Room
import com.example.anderson.projetonetworkpersistencia.dao.PersonagemDataBase


/**
 * Created by Anderson on 04/08/2018.
 */
class MyApplication : Application(){

    companion object {
        var database: PersonagemDataBase? = null
    }

    override fun onCreate() {
        super.onCreate()

       //Conexao Banco de Dados
        database = Room.databaseBuilder(this, PersonagemDataBase::class.java, "Db_DragonBall").allowMainThreadQueries().build()

    }
}