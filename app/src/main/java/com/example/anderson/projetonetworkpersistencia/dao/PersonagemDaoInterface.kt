package com.example.anderson.projetonetworkpersistencia.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.anderson.projetonetworkpersistencia.model.Personagem


/**
 * Created by Anderson on 04/08/2018.
 */

@Dao
interface PersonagemDaoInterface {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addPersonagem( vararg personagem: Personagem)

    @Query("SELECT * FROM Personagem")
    fun getPersonagens() : List<Personagem>
}