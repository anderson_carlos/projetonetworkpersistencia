package com.example.anderson.projetonetworkpersistencia.retrofit

import com.example.anderson.projetonetworkpersistencia.model.Personagem
import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by Anderson on 03/08/2018.
 */
interface RetrofitInterface {

    @GET("rrklc")
    fun getPersonagem() : Call<Personagem>
}