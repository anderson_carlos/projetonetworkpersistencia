package com.example.anderson.projetonetworkpersistencia.retrofit

import com.example.anderson.projetonetworkpersistencia.model.Personagem
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Anderson on 03/08/2018.
 */
class ApiRetrofit {

    companion object {


        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.myjson.com/bins/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        val personagem = retrofit.create(RetrofitInterface::class.java)

        fun getPersonagem(call: Callback<Personagem>) {
            personagem.getPersonagem().enqueue(call)
        }

    }
}