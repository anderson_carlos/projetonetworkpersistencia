package com.example.anderson.projetonetworkpersistencia.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Anderson on 03/08/2018.
 */
@Entity
data class Personagem (@PrimaryKey (autoGenerate = true)var id: Long, var nome : String, var apelido: String, var raca: String, var sexo : String, var image: String) {
}