package com.example.anderson.projetonetworkpersistencia.dao
import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.anderson.projetonetworkpersistencia.model.Personagem

/**
 * Created by Anderson on 04/08/2018.
 */
@Database(entities = arrayOf(Personagem::class), version = 1 ,exportSchema = false)
abstract class PersonagemDataBase : RoomDatabase() {

    abstract fun personagemDaoInterface(): PersonagemDaoInterface

}